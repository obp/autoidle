/*
 * Copyright (C) 2023 Affe Null
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <errno.h>
#include <fcntl.h>
#include <glob.h>
#include <linux/input.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define EV_BUF_SIZE 8
#define SCREEN_POLL_INTERVAL 10
#define SCREEN_WAKEUP_WAIT_TIME 2

void wake_lock(void)
{
	FILE *f = fopen("/sys/power/wake_lock", "w");
	if (!f) {
		perror("Failed to get wakelock");
	} else {
		fputs("autoidle\n", f);
		fclose(f);
	}
}

void wake_unlock(void)
{
	FILE *f = fopen("/sys/power/wake_unlock", "w");
	if (!f) {
		perror("Failed to release wakelock");
	} else {
		fputs("autoidle\n", f);
		fclose(f);
	}
}

void spawn_script(const char *state)
{
	int ws;

	switch (fork()) {
	case 0:
		execlp("autoidle-helper", "autoidle-helper", state, (char*) NULL);
		perror("Failed to execute autoidle-helper");
		exit(1);
	case -1:
		perror("Failed to fork");
		break;
	default:
		break;
	}

	if (wait(&ws) < 0)
		perror("Failed to wait");
	else if (WIFEXITED(ws) && WEXITSTATUS(ws) != 0)
		printf("helper exited with status %d\n", WEXITSTATUS(ws));
	else if (WIFSIGNALED(ws))
		printf("helper killed by signal %d\n", WTERMSIG(ws));
	else if (WIFSTOPPED(ws))
		printf("helper stopped unexpectedly\n");
	else if (WIFCONTINUED(ws))
		printf("helper continued unexpectedly\n");
}

void check_display(void)
{
	glob_t gb;
	bool on = false;

	sleep(SCREEN_WAKEUP_WAIT_TIME);

	glob("/sys/class/drm/card*-*/dpms", GLOB_NOSORT, NULL, &gb);
	for (; gb.gl_pathc > 0; gb.gl_pathc--) {
		FILE *f;
		char buf[4];

		f = fopen(gb.gl_pathv[gb.gl_pathc-1], "r");
		if (f) {
			buf[fread(buf, 1, 3, f)] = 0;
			if (buf[0] == 'O' && buf[1] == 'n' && buf[2] == '\n') {
				on = true;
				break;
			}
			fclose(f);
		} else {
			perror(gb.gl_pathv[gb.gl_pathc-1]);
		}
	}
	globfree(&gb);

	if (on)
		spawn_script("resume");
	else
		spawn_script("idle");
}

int main(void)
{
	glob_t gb;
	struct epoll_event ev, events[EV_BUF_SIZE];
	int epollfd, timerfd, i, nfds;
	struct itimerspec timerspec = {
		.it_interval = {
			.tv_sec = SCREEN_POLL_INTERVAL,
		},
		.it_value = {
			.tv_sec = SCREEN_POLL_INTERVAL,
		},
	};

	timerfd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
	if (timerfd < 0) {
		perror("Failed to create timerfd");
		return 1;
	}

	if (timerfd_settime(timerfd, 0, &timerspec, NULL) < 0) {
		perror("Failed to set timer");
		return 1;
	}

	epollfd = epoll_create1(EPOLL_CLOEXEC);
	if (epollfd < 0) {
		perror("Failed to create epollfd");
		return 1;
	}

	ev.events = EPOLLIN;
	ev.data.fd = timerfd;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, timerfd, &ev) < 0) {
		perror("Failed to add timerfd to epoll");
		return 1;
	}

	sleep(SCREEN_WAKEUP_WAIT_TIME);

	/*
	 * EPOLLWAKEUP on all input devices. Doing this in the UI seems
	 * to be too complicated (would require library changes), so
	 * we do it here to prevent the device from falling asleep when
	 * a key is pressed.
	 */
	glob("/dev/input/event*", GLOB_NOSORT, NULL, &gb);
	ev.events |= EPOLLWAKEUP;
	for (; gb.gl_pathc > 0; gb.gl_pathc--) {
		ev.data.fd = open(gb.gl_pathv[gb.gl_pathc-1], O_RDONLY);
		if (ev.data.fd >= 0) {
			if (epoll_ctl(epollfd, EPOLL_CTL_ADD, ev.data.fd, &ev) < 0) {
				perror("Failed to add input fd to epoll");
			}
		} else {
			perror(gb.gl_pathv[gb.gl_pathc-1]);
		}
	}
	globfree(&gb);

	while (1) {
		wake_unlock();
		nfds = epoll_wait(epollfd, events, EV_BUF_SIZE, -1);
		wake_lock();
		if (nfds < 0 && errno != EINTR) {
			perror("epoll_wait");
			return 1;
		}
		for (i = 0; i < nfds; i++) {
			if (events[i].data.fd == timerfd) {
				uint64_t time;
				read(timerfd, &time, sizeof(time));
			} else {
				struct input_event inp;
				read(events[i].data.fd, &time, sizeof(inp));
			}
		}
		check_display();
	}

	return 0;
}
