# autoidle

This is a hack for saving power when a mobile device is not being used.
The `autoidle` daemon runs as root and checks the power status of all
connected displays every 10 seconds. If the status is "Off" for all
displays, the Linux kernel's autosleep feature is enabled and all CPUs
except CPU0 are powered off. When the phone wakes up, this daemon keeps
the device awake for 2 seconds and then checks the status again,
disabling sleep and powering up CPUs if needed.

Do not use this daemon if you want to use the device with the display
turned off.

The durations can be changed by editing the defines in `autoidle.c`.

## Compiling

```
gcc -O2 autoidle.c -o autoidle
```

## Installing

```
install -Dm755 autoidle -t /usr/bin
install -Dm755 autoidle-helper -t /usr/bin
```
